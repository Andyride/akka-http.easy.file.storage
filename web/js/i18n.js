// Локализация
function i18nGetLang() {  return i18n.lang; }

function i18nGet(Name) {
  if( i18n.Locale[Name][i18n.lang][i] ) return i18n.Locale[Name][i18n.lang][i];
  return i18n.Locale[Name][i18n.lang];      
}

// ************************************************* 
let i18n={
  selfName:"i18n",  
  lang:"",  
  defaultlang:"en-US",
  Languages:{ "en-US":"en", "ru-RU":"ru"},  
  Init: function() { //console.log(this.SelectLang);
//      alert(this.SelectLang.tagName);
     this.SelectLang.onchange=function(){ i18n.Set(this.value);  document.activeElement.blur(); }
     for(l in this.Languages ){
       let newOption = new Option(this.Languages[l], l);
       this.SelectLang.appendChild(newOption);
     }
     if(this.Languages[navigator.language])this.lang=navigator.language;
     else this.lang=this.defaultlang; 
     this.SelectLang.value=this.lang;
     this.Set(this.lang);
  },  
  Set: function(Lang='') { this.lang=Lang;  
//     let els=document.getElementsByName("Face");
	let allElements = document.getElementsByTagName('*');
	for(k in allElements){ 
	  if(!allElements[k].getAttribute)continue;
	  let valattr=allElements[k].getAttribute("i18n");
	  if( valattr && this.Locale[valattr] ){ 
	    let el=allElements[k]; 
	    if( el.tagName=="INPUT" )el.value=this.Locale[valattr][Lang]
	    else el.innerHTML=this.Locale[valattr][Lang]
	  }
	}
	if(typeof(Mode)!='undefined' && Mode['Focus'] && Mode['Focus'].selfName=="ListUsersCommands")ListUsers.Update();
  },
  Get: function(Name, i=-1) { 
    if( this.Locale[Name][this.lang][i] ) return this.Locale[Name][this.lang][i];
    return this.Locale[Name][this.lang];    
  },
  Locale:{
  Lang:{"en-US":"Language : ", "ru-RU":"Язык : "},
  Header:{"en-US":"Easy file storage", "ru-RU":"Простое файловое хранилище"},
  registration:{"en-US":"Registration", "ru-RU":"Регистрация"},
  Name:{"en-US":"Name : ", "ru-RU":"Имя : "},
  Pass:{"en-US":"Password : ", "ru-RU":"Пароль : "},
  Group:{"en-US":"Group : ", "ru-RU":"Группа : "},
  Lock:{"en-US":"Lock : ", "ru-RU":"Блокировка : "},
  Enter:{"en-US":"Enter", "ru-RU":"Вход"},
  ListUsers:{"en-US":"List users", "ru-RU":"Список пользователей"},
  MkUser:{"en-US":"Create user", "ru-RU":"Создать пользователя"},
  DelUser:{"en-US":"Delete user", "ru-RU":"Удалить пользователя"},
  UpdateList:{"en-US":"Update list", "ru-RU":"Обновить список"},
  MyFiles:{"en-US":"My files", "ru-RU":"Мои файлы"},
  Upload:{"en-US":"Upload", "ru-RU":"Загрузить"},
  DownLoad:{"en-US":"Download", "ru-RU":"Скачать"},
  DelFile:{"en-US":"Delete", "ru-RU":"Удалить"},
  MkDir:{"en-US":"Create directory", "ru-RU":"Создать каталог"},
  Info:{"en-US":"Server info", "ru-RU":"Инфо сервера"},
  ChPass:{"en-US":"Change password", "ru-RU":"Сменить пароль"},
  SelfDel:{"en-US":"Delete self", "ru-RU":"Удалить себя"},
  Exit:{"en-US":"Exit", "ru-RU":"Выход"},
  ShowHidePass:{"en-US":"Show/Hide password", "ru-RU":"Показать/скрыть пароль"},
  MaxFileSize:{"en-US":"Max file size ", "ru-RU":"Размер файла до "},
  MaxFileName:{"en-US":"Max file name length ", "ru-RU":"Длина имени файла до "},
  AuthInfo:{"en-US":"User ", "ru-RU":"Пользователь "},
  emptyData:{"en-US":"Empty value !!!", "ru-RU":"Пустые значения недопускаются !!!"},
  messSelfDel:{"en-US":"Do you want to remove yourself?", "ru-RU":"Вы хотите удалить себя?"},
  messChangePassword:{"en-US":"New password", "ru-RU":"Новый пароль"},
  errUploadSize:{"en-US":"Too big file", "ru-RU":"Слишком большой файл "},
  errUploadName:{"en-US":"Too big name", "ru-RU":"Слишком длинное имя "},
  messUploadFiles:{"en-US":"Load ", "ru-RU":"Загружаем файл "},
  messMkdir:{"en-US":"New directory name", "ru-RU":"Введите имя каталога"},
  errSelect:{"en-US":"Select row", "ru-RU":"Выберите элемент"},
  messDownLoad:{"en-US":"Download ", "ru-RU":"Получаем файл "},
  errDownLoad:{"en-US":"Error download ", "ru-RU":"Ошибка загрузки файла "},
  messSelectUser:{"en-US":"Select user", "ru-RU":"Выберите пользователя"},
  messRmUser:{"en-US":"Delete user : ", "ru-RU":"Удалить пользователя : "},
  messRmFile:{"en-US":"Delete : ", "ru-RU":"Удалить : "},
  NewUser:{"en-US":"New user :", "ru-RU":"Новый пользователь :"},
  Close:{"en-US":"Close", "ru-RU":"Закрыть"},
  Create:{"en-US":"Create", "ru-RU":"Создать"},
  Save:{"en-US":"Save", "ru-RU":"Сохранить"},
  orderCol:{"en-US":{name:"Name", password:"Password", gid:"Group", lock:"Lock"},
	    "ru-RU":{name:"Имя", password:"Пароль", gid:"Группа", lock:"Блокировка"}    
  },
  retCodes:{
    "en-US":["Ok", "No such user ", "Bad name ", "Wrong password ", "Bad password ", "Empty value",
	      "No permissions", "User locked", "User exist", "Not found ", "Delete error ", "Invalid file name ",
	      "Error creating directory ", "Error reading directory ", "File upload error "],
    "ru-RU":["Ok", "Неверное имя ", "Плохое имя ", "Неверный пароль ", "Плохой пароль ", "Пустое имя или парль не допускается",
	      "Нет прав доступа", "Пользователь заблокирован", "Пользователь уже существует", "Ресурс не найден ",
	      "Ошибка удаления ", "Недопустимое имя файла ", "Ошибка создания каталога ", "Ошибка чтения каталога ",
	      "Ошибка загрузки файла "]
    }
  }
}

