//  Общение с окнами
// Создать новое окно
function NewWindow(url='', name=''){
  let WinParam="title=no,location=no,menubar=no,toolbar=no,status=no,width=600,height=400,top=150,left=250";
  return window.open(url, name, WinParam );
}
// Получить текущего пользователя из таблицы
function GetEditUser(){
  if( !ListUsers.Table.Crow.node ) return {};
    let i=ListUsers.Table.Crow.node.id;    
    return ResponceData.users[i];
//   console.log(UserList.Table);
}


//************************  Создание пользователя админом
// Создать нового пользователя из окна регистрации
function CreateUser(NewUser){  NewUserReg.Send(NewUser)  } 

let NewUserReg={// 
  Send: function( user ) {   
      if(!user.name || !user.password){alert(i18n.Get("emptyData")); return}
      let params=RequestProps("POST", true );
      params["body"]=JSON.stringify(user);
      SendRequest("/amkuser", params, this )
  },
  Get:  function(data) { //alert(data.status.message)
    ListUsersCommands.Action("listusers");
  }  
}

//******************************** Сохранить данные пользователя из окна редактирования
function SaveEditUser(NewUser){  ChangeUser.Send(NewUser) }

// Изменить данные пользователя имя пользователя не меняется и уникально
let ChangeUser={
  Send: function( NewUser ) {   
  let params=RequestProps("POST", true );
  params["body"]=JSON.stringify(NewUser); 
  SendRequest("/changeuser", params, this )
  },
  Get:  function(data) { //alert(data.status.message);
    ListUsersCommands.Action("listusers");
  }  
}

// ******************************** Команды списка пользователей
let ListUsersCommands={
  selfName:"ListUsersCommands",  
  allName:"All",
  Action: function( parm ) { // Выполнить команду или запрос к серверу
    switch(parm) { 
		    case 'newUser': NewWindow("/newuser.html", "newUser");
		    break;
		    case "rmuser": this.UserAct(parm);
		    break;
		    case "listusers": SendRequest("/listusers", RequestProps(), this);
		    break;
		    default: //alert(act);
	  }
  },
  Set: function() { // Вывести команды мои файлы    
    myFilesPath.panePath.innerHTML="";    
    ListUsers.Set();    
    this.Action("listusers");
  },
  KeyHandler: function(event) { // Обработка клавиш
    const keyName = event.key;
    
    switch(keyName) {   
	case 'Delete':this.UserAct('rmuser');
	break;
	case 'Insert':this.Action('newUser');
	break;
	case 'Enter':this.TwoClick();
	break;
	case 'ArrowUp':this.SetSelection('prev');
	break;
	case 'ArrowDown':this.SetSelection('next');
	break;
	default: //alert(KCode);
    }    
  },
  SetSelection:function( np ){ 
    let cel=ListUsers.Table.Crow.node;
    if(!cel)return;
    let target={};let UpDown=true;
    switch(np){
	case 'next':target=cel.nextSibling;UpDown=false;
	break;
	case 'prev':target=cel.previousSibling;	
	break;
	default: //alert(KCode);      
    }     
    if(target){ListUsers.Table.SetSelection(target);target.scrollIntoView(UpDown)}
  },
   //   Double click
  TwoClick:function(){ 
	  let row=ListUsers.Table.Crow.node;
	  if( !row ){ return };    
	  if(row.name=="head")return;	  
	  if(row.tagName=="TR") NewWindow("/editusers.html", "editUser");
  },
    //    Удалить пользователя
  UserAct: function (act){ 
    //     Получить индекс пользователя
    if( !ListUsers.Table.Crow.node ){alert(i18n.Get("messSelectUser")); return };
    let i=ListUsers.Table.Crow.node.id;    
    let user=ResponceData.users[i];
    let mess="";
    switch(act) {   
      case 'rmuser': mess=i18n.Get("messRmUser");
      break;
      default: //alert(KCode);
    }
    if( confirm(mess + user.name) ){
      let params=RequestProps("POST", true );
      params["body"]=JSON.stringify( user ); 
//       console.log(params)
      SendRequest("/"+act, params, this)
    }
  },
  // Обработка ответа с сервера
  Get: function(data){ 
    switch(data.req) {
	case 'rmuser': this.Action("listusers");
	   break;	
	case 'listusers': ListUsers.Update();
	      break;	
	default: alert(data.req);
    }
  },
}

// ************************************************* Список пользователей
let ListUsers={
  selfName:"ListUsers",  
  allName:"All",
  Set: function() { // Вывести список файлов и путь
      this.ListPane.innerHTML="";
      this["Table"]=MkTable(this.ListPane, this);     
  },  
   //   Обновить таблицу
  Update: function(){ 
     let content=ResponceData.users;  
     let ftable=this["Table"];
     let ind=-1;
     if(ftable.Crow.node)ind=ftable.Crow.node.rowIndex;
     ftable.innerHTML="";
     let order=i18n.Get("orderCol");//{name:"Имя", password:"Пароль", gid:"Группа", lock:"Блокировка"}
    //   Заголовок
    let head = document.createElement("tr");  head.name="head";
    for(h in order){
	let cell = document.createElement("th"); cell.innerHTML=order[h]+" &dArr;";
	head.appendChild(cell);    
    }
    ftable.appendChild(head);
//   Содержимое
    for( c in content){
      let row = document.createElement("tr");row.align="center";
      row.id=c; 
      for(h in order){
	let cell = document.createElement("td"); cell.innerHTML=content[c][h];
	row.appendChild(cell);    
      }
    ftable.appendChild(row);
    }    
    if(ftable.rows.length>0){
      ftable.sortData(ftable.sortIndex); 
      if(ind<0){ ftable.SetSelection(ftable.rows[1]); return}
      if(ind>0 && ind<ftable.rows.length) ftable.SetSelection(ftable.rows[ind]); 
      else  ftable.SetSelection(ftable.rows[ftable.rows.length-1]);
    }
  },
}

//*********************************  Таблица списка пользователей
function MkTable( parent, owner){
  let newTab = document.createElement("table");
  newTab.id="listuser";
  newTab.style=" padding: 0 20px; width: 70%; -moz-user-select: none; -webkit-user-select: none;"
//   newTab.border="1px";  
  newTab.Owner=owner;
  newTab.sortIndex=0;
//   Selection
  newTab.Crow={};  //   Текущая строка
  newTab.SetSelection= function(row){ 
    if(row.name=="head")return;
//     Снять с выделение со старой строки
    if( this.Crow["node"] )this.Crow["node"].style.backgroundColor = this.Crow["bColor"];
//     Установить на новую
    this.Crow["node"]=row; this.Crow["bColor"]=row.style.backgroundColor;
    row.style.backgroundColor = "lightblue";
    Mode['Focus']=ListUsersCommands;
  }

  // Сортировка таблицы
  newTab.sortData= function(iCell){
    let sortedRows = Array.from(this.rows).slice(1)
    .sort((rowA, rowB) => rowA.cells[iCell].innerText > rowB.cells[iCell].innerText ? 1 : -1);
    for( row in sortedRows )this.appendChild(sortedRows[row]); 
    this.Owner["SortIndex"]=iCell; //console.log(this.Owner);
  }
//   Обработка событий
  newTab.onclick= function(event){ 
	  let row=event.target.parentNode; 
// 	  Если заголовок сортируем
	  if(row.name=="head") {this.sortData(event.target.cellIndex); return;}
	  if(row==this.Crow["node"]){  ListUsersCommands.TwoClick(); return }	
	  // 	  Если строка
	  if(row.tagName=="TR"){  newTab.SetSelection(row); }
  }
//   newTab.ondblclick= function(event){ ListUsersCommands.TwoClick();  }    
   parent.appendChild(newTab);
   return newTab;
}

