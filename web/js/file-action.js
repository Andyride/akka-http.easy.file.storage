// Загрузка в окно
function GetDownload(){ return  myFilesCommands.OpenDocumenObj }

// Уведомление о загрузке файла
let DownUpNotice={
  selfName:"DownUpNotice",  
  Init: function(){
      this.noticedu.onclick=function(){ DownUpNotice.Hide(0) }
  },
  Show: function(msg){
    this.noticedu.innerHTML=msg;
    this.noticedu.style.display='block';
  },
  Hide: function(duration){ 
    setTimeout(
	function(){ 
	this.noticedu.innerHTML="";
	this.noticedu.style.display='none';
	},
	duration
    );
  },
}


// Управление файлами пользователя
// ************************************************* 
let myFilesCommands={
  selfName:"myFilesCommands",  
  allName:"All",  
  Set: function() { // Вывести команды мои файлы
    this.UploadFiles.onchange= function(){ // upload handler
	      let command="/upload"+myFilesPath.Path.join("/");      
	      	      
	      for (i in this.files) { 
		  const formData = new FormData();
		  let CountFiles=0; let errSize=""; let errName="";
// 		
		  if(this.files[i].size){//console.log(this.files[i])
		      if(myFilesPath.maxFileSize < this.files[i].size){
			if(errSize)errSize+=this.files[i].name+","
			else errSize=i18n.Get('errUploadSize')+this.files[i].name+",";			
		      }else
		      if(myFilesPath.maxLengthFileName < this.files[i].name.length){ 
			if(errName)errName+=this.files[i].name+","
			else errName=i18n.Get('errUploadName')+this.files[i].name+","
		      }
		      else { formData.append('files', this.files[i]); CountFiles++; }
		  }
// 		  formData.append('files', this.files[i]);
		  if(errName+errSize)tempAlert(errSize+errName,4000);
		  if(CountFiles==0)continue;
		  let FileName=this.files[i].name;
		  DownUpNotice.Show(i18n.Get('messUploadFiles')+FileName+" <img src='img/tenor.gif' height='16' >");
		  let Props= {method: 'POST',
			      headers:{'Authorization': 'Basic ' + utf8_to_b64(Auth.user.name + ":" + Auth.user.password) },
			      body: formData
			      };
		   SendRequest( command, Props, myFilesCommands )	  
	      };
	  }    
    myFilesPath.Path=[];
    myFilesList.Set();      
//     Команда по умолчанию
    this.Dir();
  },  
  KeyHandler: function(event) { // Обработка клавиш
     const keyName = event.key;
    if(event.altKey){
      switch( keyName ) {   
	  case "Insert": this.Mkdir();
	  break;
// 	  case "d": this.DownLoad();
// 	  break;
// 	  case "u": this.UpLoad();
// 	  break;
	  default:
      }
      return;
    }
    switch(keyName) {   
	case 'Delete':this.Rm();
	break;
	case 'Insert':this.UpLoad();	
	break;
	case 'Enter':this.TwoClick();
	break;
	case 'ArrowUp':this.SetSelection('prev');
	break;
	case 'ArrowDown':this.SetSelection('next');
	break;
	case 'Home':this.SetSelection('Home');
	break;
	case 'End':this.SetSelection('End');
	break;
// 	case 'PageDown':this.SetSelection('PageDown');
// 	break;
// 	case 'PageUp':this.SetSelection('PageUp');
// 	break;
	default:// alert(keyName);
    }
  },
  SetSelection:function( np ){ 
    let cel=myFilesList.Table.Crow.node;
    let Rows=myFilesList.Table.rows;
    if(!cel)return;
    let target={}; let UpDown=true;
    switch(np){
	case 'next':target=cel.nextSibling;UpDown=false;
	break;
	case 'prev':target=cel.previousSibling;	
	break;
	case 'Home':target=Rows[0];	
	break;
	case 'End':target=Rows[Rows.length-1];	
	break;
	default: //alert(KCode);      
    }     
    if(target){myFilesList.Table.SetSelection(target);target.scrollIntoView(UpDown)}
  },
  //   Double click
  TwoClick:function(){ 
	  let row=myFilesList.Table.Crow.node;
	  if( !row ){ return };    
	  // 	  Если каталог открываем содержимое	   
	  if(row.isF==0){
// 	    Если Up переходим на уровень выше
	    if(row.id=="Up") myFilesPath.Path.pop(); else myFilesPath.Path.push(row.name);
	    myFilesCommands.Dir();
	  }else{
// 	  Если файл грузим
	    myFilesCommands.OpenDoc(); }
  },
  //   Создать каталог
  Mkdir:function( Name=""){ 
    Name = prompt(i18n.Get('messMkdir'),Name);
    if(!Name)return;
    let command="/mkdir"+myFilesPath.Path.join("/")+"/"+Name;
      SendRequest(command, RequestProps(), this);
  },
  //   Открыть документ
  OpenDoc:function(){ 
      if( !myFilesList.Table.Crow.node ){alert(i18n.Get("errSelect")); return };
      let fname=myFilesList.Table.Crow.node.name;   
      let command="/opendoc"+myFilesPath.Path.join("/")+"/"+fname;
      this["OpenDocumenObj"]={ user:Auth.user, path: command };
      NewWindow("/opendoc.html", "OpenDoc");
  },  
//   Удалить
  Rm:function(){ 
      if( !myFilesList.Table.Crow.node ){alert(i18n.Get("errSelect")); return };
      let fname=myFilesList.Table.Crow.node.name;   
      let command="/rm"+myFilesPath.Path.join("/")+"/"+fname;
      if( confirm(i18n.Get("messRmFile") + fname +" ?" ) ) SendRequest(command, RequestProps(), this);      
  },
  Dir:function(){ 	// Получить Список файлов 
      SendRequest("/dir"+myFilesPath.Path.join("/"), RequestProps(), this);    
  },    
//   
  UpLoad:function(){ this.UploadFiles.click()  },  
//   
  DownLoad: async function(){ 
    if( !myFilesList.Table.Crow.node ){alert(i18n.Get("errSelect")); return };
    if(myFilesList.Table.Crow.node.isF==0)return;
    let Name=myFilesList.Table.Crow.node.name;     
    let command=StateS.baseURL+"/download"+encodeURI(myFilesPath.Path.join("/")+"/"+Name);
//           console.log(command);
    props={"headers":{'Authorization': 'Basic ' + utf8_to_b64(Auth.user.name + ":" + Auth.user.password) } }
//     tempAlert("Получаем файл "+Name);
    let CountBytes=0;
    fetch(command, props)
    .then(response => response.body)
    .then(body =>
	{ const reader = body.getReader();
	  return new ReadableStream({
	      start(controller){
		return pump();
		function pump() {
		    return reader.read().then(({ done, value }) => {
		      if (done) {  controller.close();  return;  }
		      controller.enqueue(value);
		      CountBytes+=value.byteLength;
		      DownUpNotice.Show(i18n.Get("messDownLoad")+Name+" : "+CountBytes);
		      return pump();
		      });
		}
	      }
	  })
    })
    .then(stream => new Response(stream))
    .then(response => response.blob())
    .then(blob => {
		const url = window.URL.createObjectURL(blob);
		this["DownLoadHref"].href = url;
		// the filename you want
		this["DownLoadHref"].download = Name;
		DownUpNotice.Hide(3000);
// 		document.body.appendChild(a);
		this["DownLoadHref"].click();
		window.URL.revokeObjectURL(url);
	  })
    .catch(error => {DownUpNotice.Hide(3000); tempAlert(i18n.Get("errDownLoad")+error)}  );
  },
  
// Результат загрузки на сервер
  RespUpload: function(){ 
    DownUpNotice.Hide(0);this.Dir(); 
  },
  
// Обработка ответа с сервера
  Get: function(data){ DownUpNotice.Hide(0);
    if(!data.status)return;
    switch(data.req) {
		    case 'dir': myFilesPath.Set();myFilesList.Update('dir');
		      break;	
		    case 'rm': 
		    case "mkdir": 
		    case 'upload': this.RespUpload();
		      break;		      
		    default: //alert(data.req);
    }
  },
}

//************************************************************* Путь по каталогу
let myFilesPath={
  selfName:"myFilesPath",  
  Path:[],
  maxFileSize:0,
  maxLengthFileName:0,
//   
  Action: function( i ) { // Переход по путям
    if(i+1 < myFilesPath.Path.length ){ 
      myFilesPath.Path.length=i+1;
      myFilesCommands.Dir();
    }
//     alert(parm)    
  },
  Set: function() { // Вывести путь
	this.maxFileSize=ResponceData.maxFileSize;
	this.maxLengthFileName=ResponceData.maxLengthFileName;
	document.getElementById("maxFileSize").innerHTML=this.maxFileSize/(1024*1024)+" Mb";
	document.getElementById("MaxFileName").innerHTML=this.maxLengthFileName;
	let cont=this.panePath;
	cont.innerHTML=" ";
	this.Path=ResponceData.path.split("/");    
    cont.onclick=function(event){ myFilesPath.Action(parseInt(event.target.id)) }; 
    for( i in this.Path){
      ahref=document.createElement("a");
      ahref.id=i;
      ahref.href="#";
      ahref.style="margin: 0 3px;"    
      ahref.innerHTML=this.Path[i]; 
      if(i==0)ahref.innerHTML='..';     
      cont.innerHTML+=" / "; //ahref.onclick=function(){ myFilesPath.Action(this.id) };
      cont.appendChild(ahref);
    }
  },  
}


// ******************************************Список файлов
let myFilesList={
  selfName:"myFilesList",  
  allName:"All",
  Path:[],  
  Set: function() {
      this.ListPane.innerHTML="";
      this["Table"]=FilesTable( this.ListPane, this);     
  },  
  //   Обновить таблицу
  Update: function (parm=''){ 
    let content=ResponceData.flist;   
    let ftable=this.Table;
    let ind=-1;
    if(ftable.Crow.node)ind=ftable.Crow.node.rowIndex;
    content.sort(function(a, b) { return (b.name < a.name) ? 1 : -1}).sort(function(a, b) { return (b.isfile < a.isfile) ? 1 : -1 });
//    console.log(content);
    ftable.innerHTML="";
//     Если каталог не корневой добавить Up
    if(myFilesPath.Path.length>1){
      let up=document.createElement("tr");
      up.id="Up";  up.isF=0;
      let cellup = document.createElement("td"); cellup.width = '10%'; 
      up.appendChild(cellup);
      cellup = document.createElement("td"); cellup.id="Up";cellup.innerHTML=". .";
      up.appendChild(cellup);
      ftable.appendChild(up);
    }
    
    for( c in content){
      let row = document.createElement("tr"); //row.align="center";
      row.id=c; row.name=content[c]["name"]; row.isF=content[c]["isfile"];
      //       Картинка
      let cell = document.createElement("td");
      cell.width = '10%'; cell.id="img"; // cell.name=content[c]["name"];cell.isF=content[c]["isfile"];
      let ico=document.createElement("img");
      if(content[c]["isfile"]==0){ ico.src = "/img/dir.png"; }else{ ico.src = "/img/file.png"; }
      ico.width="16"; cell.appendChild(ico);	
      row.appendChild(cell); 
//    Содержание
      cell = document.createElement("td"); cell.innerHTML=content[c]["name"]; // cell.name=content[c]["name"];
      cell.isF=content[c]["isfile"];
      row.appendChild(cell); 
      ftable.appendChild(row);
    }      
    ftable.Crow={};
    if(ftable.rows.length>0){ 
      if(ind<0 || parm=='dir'){ ftable.SetSelection(ftable.rows[0]); return}
      if(ind>=0 && ind<ftable.rows.length) ftable.SetSelection(ftable.rows[ind]); 
      else  ftable.SetSelection(ftable.rows[ftable.rows.length-1]);
    }
  }
}

//*********************************  Таблица списка файлов
function FilesTable( parent, owner){
  let newTab = document.createElement("table");
  newTab.id="flist";
  newTab.tabindex=4;
  newTab.style="padding: 0 20px; -moz-user-select: none; -webkit-user-select: none;"
//   newTab.border="1px";  
  newTab.Owner=owner;
//   Selection
  newTab.Crow={};  //   Текущая строка
  newTab.SetSelection= function(row){ 
//     Снять с выделение со старой строки
    if( this.Crow["node"] )this.Crow["node"].style.backgroundColor = this.Crow["bColor"];
//     Установить на новую
    this.Crow["node"]=row; this.Crow["bColor"]=row.style.backgroundColor;
    row.style.backgroundColor = "lightblue";
    Mode['Focus']=myFilesCommands;
  }
//   Обработка событий
  newTab.onclick= function(event){ 	
	let targ=event.target;
	let row=targ.parentNode; //console.log(row);	
	if(row.id=="img")row=targ.parentNode.parentNode;
	if(row==this.Crow["node"]){  myFilesCommands.TwoClick(); return }	
	if(row.tagName=="TR"){  newTab.SetSelection(row); }
  }
//   
//   newTab.ondblclick= function(event){  myFilesCommands.TwoClick();  }
  parent.appendChild(newTab);
  return newTab;
}
