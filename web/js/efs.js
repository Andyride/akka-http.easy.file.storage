// Объект состояния системы
let StateS={}

//  Всплывающие сообщения
function tempAlert(msg,duration=3000) {
 var el = document.getElementById("notice");
 if(el.style.display == "block")el.innerHTML += "<br>"+msg;
 else el.innerHTML = msg;
 el.style.display = "block";
 setTimeout(function(){  el.style.display = "none"; },duration);
}

// ************************************
function utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}


//*********************************************  Дествия
function Action(msg) {
  document.activeElement.blur();
  switch(msg) {   
		    case 'reg': NewWindow("/reguser.html", "newUser");
		    break;
		    case "enter": Enter.Send();
		    break;
		    case 'Files': Mode.Set('Files'); 
		    break;
		    case 'Users': Mode.Set('Users');
		    break;
		    case "exit":location.reload(true);
		    break;
		    case "selfdel": SelfDel.Send();
		    break;
		    case "chpass": ChangePassword.Send();
		    break;
		    case "listusers": ListUsersCommands.Set();
		    break;
		    case "info": Info.Send();
		    break;
		    case 'upload': myFilesCommands.UpLoad(); // загрузка файла на сервер
		    break;
		    case 'download': myFilesCommands.DownLoad(); //this.OpenDoc(); // скачать файл с сервера
		    break;
		    case "rm": myFilesCommands.Rm(); // удалить файл или каталог
		    break;
		    case "mkdir": myFilesCommands.Mkdir(); // создать каталог
		    break;
		    case "dir": myFilesCommands.Dir(); // обновить список файлов
		    break;
		    case 'newUser': NewWindow("/newuser.html", "newUser"); // создать пользователя
		    break;
		    case "rmuser": ListUsersCommands.UserAct(msg); // удалить пользователя
		    break;
		    default: //alert("Неизвестный параметр :"+msg);
	  }
}


function KeyHandler(event){
  
    if( event.key=='Enter'){
//       console.log(event.target.tagName);event.stopPropagation();
      if(event.target.tagName=="INPUT"){ return; }
    }
    
//     console.log(event);
//   const keyName = event.key;
  
//   if(event.altKey){
//       switch( keyName ) {   
// 	  case "u": Action('Users');
// 	  break;
// 	  case "f": Action('Files');
// 	  break;      
// 	  default: tempAlert(keyName);
//       }      
//   }
  
  if( Mode['Focus'] ) Mode['Focus'].KeyHandler(event);// alert(event.code); }
}

// Объект для аутентификации
let Auth={
  selfName:"Auth",
  user:{name:"", password:"", gid:0, lock:0 },
}

// текущий режим работы формирование пользовательского интерфейса
let Mode={
  Set: function(name) {
    switch(name) { 
	  case 'Users': this.SetUsersMode();
          break;		    
	  case 'Files': this.SetFilesMode();
          break;		    
	  }
  },
  SetUsersMode: function() { // Установка режима работы с пользователями для админа    
    HideEl("FileCL");
    if(Auth.user.gid>0)return;
    ShowEl("CC"); 
    ShowEl("UserCC"); 
    ShowEl("UserCL"); 
    ListUsersCommands.Set();
  },
  SetFilesMode: function() { // Установка режима работы с файлами
    HideEl("UserCL"); 
    if(Auth.user.gid>0)HideEl("UserCC"); 
    else ShowEl("UserCC"); 
    ShowEl("CC"); 
    ShowEl("FileCL"); 
    myFilesCommands.Set();
  }
}

//  Показать скрыть элемент
function ShowEl(id) { let el=document.getElementById(id); el.style.display='block'; }
function HideEl(id) { let el=document.getElementById(id); el.style.display='none'; }


//********************************* Действия по входу в систему 
let Enter={
   selfName:"Enter", 
   Send: function( Name='', Pass='' ) {	
	if(!Name)Name=Auth.name.value;
	if(!Pass)Pass=Auth.password.value;		
	if(!Name || !Pass){alert(i18n.Get("emptyData")); return}
	SendRequest("/enter", RequestProps("GET", true, {name:Name, password:Pass}), this )
  },
  Get: function(data) { // Успешный вход    
    Auth.user=data.user; // Зафиксировали пользователя
    Auth.authInfo.innerHTML=data.user.name; // Вывели информацию
    Auth.name.value="";
    Auth.password.value="";    
    Mode.Set('Files');
  },
}

//****************************** Удалить себя из системы
let SelfDel={
  Send: function() {
	if( confirm(i18n.Get("messSelfDel")) )SendRequest("/selfdel", RequestProps(), this )
  },
  Get: function(data) {     Action("exit");  }
}

//*********************** Сменить пароль
let ChangePassword={
  Send: function(NewPass=Auth.user.password) {
    NewPass = prompt(i18n.Get("messChangePassword"),NewPass);
    if(!NewPass)return;
    SendRequest("/chpass"+NewPass.trim(), RequestProps(), this )
  },
  Get: function(data) {     Auth.user=data.user;  }
}


// ******************************* Блок fetch обмена с сервером
// data - параметры запроса fetch
//  {
//         method: 'GET', // *GET, POST, PUT, DELETE, etc.
//         mode: 'cors', // no-cors, cors, *same-origin
//         cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
//         credentials: 'same-origin', // include, *same-origin, omit
//         headers: {
// 	    'Content-Type': 'text/html',
//             'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
//         }, 
//         redirect: 'follow', // manual, *follow, error
//         referrer: 'no-referrer', // no-referrer, *client
//         body: JSON.stringify(data), // тип данных в body должен соответвовать значению заголовка "Content-Type"
//         body: data
//   }

let ResponceData={}

function sendFetch(url = '', data = {}) {
    return fetch(url, data).then(response => response.json()); 
//     .then(response => response.text()); 
}

// Параметры запроса начальные
function RequestProps(method = 'GET', auth = true, user={} ){
  let rp={ method: method } ;
  rp["headers"]={'Content-Type':"application/json; charset=utf-8"};
  if(auth){ let userName=""; let userPassword="";    
    if( user.name && user.password){userName=user.name;  userPassword=user.password;}
    else {userName=Auth.user.name; userPassword=Auth.user.password; }
    if(userName && userPassword) rp["headers"]['Authorization']= 'Basic ' + utf8_to_b64(userName.trim() + ":" + userPassword.trim());
  }    
  return rp
}

// Послать запрос url="", props={} - параметры запроса, handler - объект обработчик ответа
function SendRequest(url="", props={}, handler ){ 
  ResponceData={};
  sendFetch( encodeURI(StateS.baseURL+url), props ) 
  .then(data => {  GetResponce(data, handler)} ) 
  .catch(error => { console.error(error); tempAlert(error);  GetResponce( {} , handler) } );
}

//  Обработка результатов запроса
function GetResponce(data, handler){
    if(!data.status){ handler.Get( {} ); return }
   if(data.status.code>0){
     DownUpNotice.Hide(0);
     let mess=i18n.Get('retCodes', data.status.code);
     if(data.message)mess+="<br>"+data.message
     tempAlert(mess);     
     return;
    }
   ResponceData=data;
   if(data.message)tempAlert(data.message);
   handler.Get(data)
}
// ******************************* 
//  Запросы
//********************************  Информация о сервере
let Info={
  Send: function() {  SendRequest("/info", RequestProps('GET',false), this ) },
  Get:  function(data) { alert(data.message) }
}

// Создать нового пользователя из окна регистрации
function NewUser(NewUser){  Reg.Send(NewUser)  } 

// *********************** Запрос на регистрацию
let Reg={// 
  Send: function( user ) {   
      if(!user.name || !user.password){alert(i18n.Get("errEnter")); return}
      let name=document.getElementById("name").value;
      let password=document.getElementById("password").value;
      let params=RequestProps("POST", false );
      params["body"]=JSON.stringify(user)
      SendRequest("/mkuser", params, this )
  },
  Get:  function(data) {
    Enter.Send(data.user.name, data.user.password );
  }  
}


// ********************************************** Изменнить вид пароля
function ChangeViewPassword(){
  let rp=document.getElementById("password");
  hr=document.getElementById("hrefPassword");
  if( rp.type=="text" ) { rp.type="password"; }
  else {rp.type="text" }
}

// Установить высоту списка
function SetHeight(newHeight=0) { 
    el=document.getElementById("ListPane");
    let top=el.offsetTop;
    let allHeight=window.innerHeight;
    newHeight=allHeight-top-10;
    if(newHeight<250)newHeight=250;
    el.style.height=newHeight+"px";
}
  
// Заполняем объекты элементами DOM
function madeObj(obj){
  let els=document.getElementsByName(obj.selfName);
  for(k in els)obj[els[k].id]=els[k];
  
  if(obj.allName){
    let els=document.getElementsByName("All");
    for(k in els)obj[els[k].id]=els[k];    
  }
  if(obj.Init)obj.Init();
}

// Инициализация
function InitSys(){
  StateS["baseURL"]=window.location.origin;
  madeObj(Auth);
//     console.log(Auth);
  madeObj(myFilesCommands);
  madeObj(myFilesList);
  madeObj(myFilesPath);
  madeObj(DownUpNotice);  
  madeObj(ListUsers);  
  madeObj(i18n);  
  document.addEventListener('keydown', function(event){ KeyHandler(event) });
  window.onresize = function(event) { SetHeight() };
}

// Инициализация
function IsLoad(){
  let checkLoad=document.getElementById("notice");
  if(checkLoad){ clearInterval(timerId);  InitSys() }  
}
let timerId = setInterval(IsLoad, 500)



