#	  AKKA-HTTP Easy file storage
      
## Introduction

This project is designed to create a simple web-based file storage.
Based on the multi-threaded AKKA-HTTP architecture.
It includes a web server (efs.jar) and the client part of html, js, css for access from a browser.
The server requires java (development was carried out on version 1.8)
The server has been tested on the Windows 7.10 and Linux platforms.
The client part is based on asynchronous processing technology "fetch",
has been tested on Mozilla Firefox 68 and Google Chrome 78 browsers.
The system is localized for Russian and English language.

## Installation and launch

Download the "EFS.zip" archive.
Unzip it to any directory.
On windows platform, run "start.bat" file.
On a Linux platform, run the "start.sh" file.
Then in the browser, enter http://localhost:8080 or http://address:8080
where "address" is the address of the computer on which the server is running.
By default, there is one user in the system

**Login: admin**

**Password: admin**

You can log in under this data, or by clicking on the "Registration" link register a new user.
After logging in you will have the opportunity to upload, download, browse (dblclick or Enter on the file name),
delete files, create and delete directories.
The "admin" user is the system administrator and can also create, delete, modify other user accounts.
System administrators are users with gid = 0.

## Stop server

On the Windows platform, press Ctrl-C in the server window, when asked about the close application, press "y".

On the Linux platform, run the "stop.sh" file, if the server was launched from the console, press Ctrl-C.

## Server settings

To change the server settings, you need to edit the conf/application.conf file
and restart the server.

"users-dir" defines the directory for storing users data, the default directory is "Users" in the current server directory

"max-content-length" defines the maximum file size to upload to server, default is set to 1024Mb

"max-length-file-name" defines the maximum size of the file name in characters, default 128

"saddress" server address, default "0.0.0.0" access from any interface

"port" server port, default is "8080"

Information on other server parameters can be found here:
<https://doc.akka.io/docs/akka-http/current/configuration.html>

## To developers

The "sbt" directory contains the sbt project, the client part of the "web" and "conf" directory will also be required.
During development, you must set the parameters
efs-> data-parameters->

"app-dir" = directory with "web" and "conf" directories

"users-dir" = directory for storing users data

in "sbt/src/main/resources/application.conf" file
