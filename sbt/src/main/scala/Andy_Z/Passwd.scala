/*
 * Copyright (C) 2019-2020 Andrey Zaytsev andyride@yandex.ru
 */

package andyz.efs

//  Passwd file
object Passwd {

// ************************* Прочитать файл
def ReadPasswd(fName: String) = {      
  var Lu: Map[String,User]=Map.empty
  import scala.io._
  try {
      val src = Source.fromFile(fName).getLines.filter(s => (s.take(1) != "#"))
      for(l <- src) {
	   var arr = l.split(":")
	   Lu+=(arr(0) -> User(arr(0), arr(1), arr(2).toInt, arr(3).toInt) )
      }
      Lu
  }catch { case ex:Exception => { println(ex.getMessage); Lu}   }
}
// ************************************************

// ************************************** Записать файл
  def WritePasswd(fName: String, ListUsers: Map[String,User] ) = {
      import java.io._
      try {
	    val pw = new PrintWriter(new File( fName ))
	    pw.write("# name: String, password: String, gid: Int, lock: Int\n")
	    for ( (k,value) <- ListUsers){
		pw.write( value.name+":"+value.password+":"+value.gid+":"+value.lock+"\n")
		}
	    pw.close        
      }catch {
		case ex:Exception => println(ex.getMessage)
	      }
  }
//   ********************************************
}
