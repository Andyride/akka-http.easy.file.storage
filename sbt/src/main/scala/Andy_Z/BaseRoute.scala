/*
 * Copyright (C) 2019-2020 Andrey Zaytsev andyride@yandex.ru
 */

package andyz.efs

import java.io.File
import java.net.{URLDecoder, URLEncoder}


import akka.actor.typed.ActorRef
import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.AskPattern._
import akka.util.Timeout

import scala.concurrent.{Future, Promise, Await}
import scala.concurrent.duration._
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import scala.util.{ Failure, Success }
// import MediaTypes._

import andyz.efs.UserHandler._
import andyz.efs.FileOper._

class BaseRoute(userHandler: ActorRef[UserHandler.Command])(implicit val system: ActorSystem[_]) {

  import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

  private implicit val timeout = Timeout.create(system.settings.config.getDuration("efs.routes.ask-timeout"))
  
//   Запросы 
  def getAuth(name: String, pass:Credentials.Provided, Cond:Int=>Boolean ): Future[RespUsr] = userHandler.ask(Auth(name, pass, Cond, _))
  def mkUser(mkuser: User, admin: Boolean): Future[RespUsr] = userHandler.ask(CreateUser(mkuser, admin, _))
  def delUser(rmuser: User): Future[RespUsr] = userHandler.ask(deleteUser(rmuser, _))
  def chUser(chuser: User): Future[RespUsr] = userHandler.ask(changeUser(chuser, _))
  def listUsers: Future[RespUsrL] = userHandler.ask(ListUsr( _))

  private implicit val context = system.executionContext 

// *************************************** Обработка пользователей админом
  def useract(command:String) = Future{
	UserAuth( (gid:Int) => gid==0 ){ Resp => 
		entity(as[User]){ json => 
		   if( Resp.status.code>0 ) complete( Resp )
		   else {
			val user=User(json.name.trim(), json.password.trim(), json.gid, json.lock)
			command match {
				  case "/amkuser" => onSuccess(mkUser(user, true)) { resp => complete( resp ) }
				  case "/changeuser" => onSuccess(chUser(user)) { resp => complete( resp ) }
				  case "/rmuser" => onSuccess(delUser(user)) { resp => complete( resp ) }
				  case _ => complete( StatusCodes.NotFound,  RespReq( Status.RnotFound, "", ""))
				}		   
		   }
		}
	}	
  }

// ******************************** get request admin
  def RequestGetAdmin(command:String) = Future{  
      UserAuth( (gid:Int) => gid==0 )
      { AuthUsr => 
	 if( AuthUsr.status.code>0 ) complete( AuthUsr ) // Ошибка аутентификации
	 else { 
	      command match {
		  case "listusers" => onSuccess( listUsers  ) { resp =>  complete( resp ) }
		  case _ => complete( StatusCodes.NotFound,  RespReq( Status.RnotFound, "", ""))
	      }
	 }
      }
  }

// ******************************** Пользовательские действия
  def RequestGetUser(command:String) = Future{ 
      UserAuth( (gid:Int) => gid>=0 )
      { AuthUsr => 
	 if( AuthUsr.status.code>0 ) complete( AuthUsr ) // Ошибка аутентификации
	 else { 
	      extractUnmatchedPath { remaining => 
		  val rem=URLDecoder.decode(remaining.toString).trim().replaceAll("""\.\.""", "")
		  command match {
		    case "/selfdel" => onSuccess(delUser(AuthUsr.user)) { resp => complete( resp ) }
		    case "/chpass" => { onSuccess( 
					  chUser(User(AuthUsr.user.name, rem, AuthUsr.user.gid, AuthUsr.user.lock))
					) { resp => complete( resp ) } }
		    case _ => complete( StatusCodes.NotFound,  RespReq( Status.RnotFound, "", ""))
		  }
	      }
	 }
      }
  }
  
// *********************************** Обработка файлов
  def fileact(command:String) = Future{ 
	UserAuth( (gid:Int) => gid>=0 ){ 
	AuthUsr => if( AuthUsr.status.code>0 ) complete(AuthUsr) // Ошибка аутентификации
		else {	extractUnmatchedPath 
			{ remaining => // путь к каталогу
				val rem=URLDecoder.decode(remaining.toString).trim().replaceAll("""\.\.""", "")
// 				val path=new String( rem.getBytes(EFSConf.FileEnc), "UTF-8" )
				val basepath=EFSConf.usersDir+"/"+AuthUsr.user.name
				command match {
				  case "/dir" => complete( FileOper.dir(basepath, rem) ) 
				  case "/rm" =>  complete( FileOper.rm(basepath, rem) )
				  case "/mkdir" => complete( FileOper.mkdir(basepath, rem) )
				  case "/download" => getFromFile(basepath + rem)
				  case "upload" => UpLoad(basepath, rem)
				  case _ => complete( StatusCodes.NotFound,  RespReq( Status.RnotFound, "", ""))
				}
			}
		      }
	}
  }

// ************************************ Get file
  def GetFile(basepath: String, rawpath: String) =Future {  
     val path=rawpath.trim().replaceAll("""\.\.""", "")
//      println(basepath+path)
      getFromFile(basepath+path)
  }
  
  // ************************************ Open document
  def OpenDoc(basepath: String, rawpath: String) =Future {
      val path=URLDecoder.decode(rawpath.toString).trim().replaceAll("""\.\.""", "")
      parameters('username, 'password) { (username, password) =>
	      val name=URLDecoder.decode(username)
	      val pass=URLDecoder.decode(password)
	      onSuccess( AuthOverParams(name, pass, (gid:Int) => gid>=0) ){ 
		rez=> if(rez.status.code>0) complete( rez ) 
		      else getFromFile(basepath+"/"+name+path)
	      }
	    
      }
  }
  
//****************************************  Информация о системе
   def getInfo: Future[String] = Future{
    val mb = 1024*1024
    val runtime = Runtime.getRuntime   
    "Active treads: " +Thread.activeCount()+"\n"+
    "Used Memory: " + (runtime.totalMemory - runtime.freeMemory) / mb+"\n"+
    "Free Memory: " + runtime.freeMemory / mb+"\n"+
    "Total Memory: " + runtime.totalMemory / mb+"\n"+
    "Max Memory: " + runtime.maxMemory / mb 
   } 
     
//******************************************   Аутентификация
//   Мда.... очень странный подход, я ведь могу сделать decode BASE64 и таки увидеть password
  def AsynAuthenticator(Cond:Int=>Boolean)(credentials: Credentials): Future[Option[RespUsr]] =
    credentials match {
      case p @ Credentials.Provided(id) =>
	Future {  
		val d1 = Duration(1000, MILLISECONDS)
		val rez=Await.result(getAuth(id, p, Cond), d1 )
		Option(rez)
	}
      case _ => Future.successful(Option( RespUsr(Status.NoName, "enter", UserHandler.EmptyUser ) ))
  
  }

  // ******************************  Аутентификация через заголовок HTTP
  def UserAuth(Cond:Int=>Boolean) = authenticateBasicAsync(realm = "secure site", AsynAuthenticator( Cond ) )
  
  // ******************************  Аутентификация через параметры
  def AuthOverParams(username:String, password:String, Cond:Int=>Boolean):Future[RespUsr] ={
	val validCredentials = BasicHttpCredentials(username, password)
	val d1 = Duration(1000, MILLISECONDS)
	val rez=Await.result( AsynAuthenticator( Cond )( Credentials(Option(validCredentials)) ), d1 )	
	rez match {
	    case Some(x) => Future.successful(x)
	    case _ => Future.successful(RespUsr(Status.NoName, "login", UserHandler.EmptyUser ))
	}
  }
  
//   *************************************
  val EfsUploadExceptionHandler = ExceptionHandler {
      case _: EntityStreamSizeException =>
//       println(s"Too big file")
      complete( StatusCodes.RequestEntityTooLarge,  RespReq( Status.erUpload, "upload", "") )
  }	
     
//***********************************   Маршрутизация   
  val userRoutes: Route = 
  concat(
    get{concat(
	pathSingleSlash { onSuccess( GetFile(EFSConf.webDir, "/index.html") ){ rez => rez }  }, // Запрос без параметров
// 	ИНформация о системе
	pathPrefix("info") { onSuccess(getInfo){ rez => complete(RespReq( Status.Ok, "info", rez))} },
// 	Вход через HTTP заголовок
	pathPrefix("enter"){ UserAuth( (gid:Int) => gid>=0 ){ Resp => complete( Resp )} },
// 	Получить список пользователей
	pathPrefix("listusers"){  onSuccess(RequestGetAdmin("listusers")) {rez => rez} },
	
// 	Операции пользователя
	pathPrefix("selfdel" | "chpass"){
	      extractMatchedPath { parm => onSuccess( RequestGetUser(parm.toString) ) { rez => rez} }
	},
	
	// Открыть документ
	pathPrefix("opendoc"){ 
	   extractUnmatchedPath { remaining => onSuccess( OpenDoc(EFSConf.usersDir,s"$remaining") ){ rez => rez } }
	},	
// 	Операции с файлами
	pathPrefix("dir" | "rm" | "mkdir" | "download"){
	      extractMatchedPath { parm => onSuccess( fileact(parm.toString) ) { rez => rez} }
	},
// 	
//   грузим файлы из web каталога
	extractUnmatchedPath { remaining => onSuccess( GetFile(EFSConf.webDir,s"$remaining") ){ rez => rez } }
    )},
//     Обработка post запросов
    post { 
      concat(
//     Регистрация нового пользователя
	pathPrefix("mkuser"){ entity(as[User]){ json => onSuccess(mkUser(json, false))	{resp => complete( resp )} } },
	
// 	Операции с пользователями
	pathPrefix("amkuser" | "changeuser" | "rmuser"){
	      extractMatchedPath { parm => onSuccess( useract(parm.toString) ) { rez => rez} }
	},
	
//     Загрузить файл
	pathPrefix("upload"){ 
		handleExceptions(EfsUploadExceptionHandler){ 
		    withSizeLimit(EFSConf.maxUploadSize) {  onSuccess( fileact("upload") ) { rez => rez } }  
		}
	}
      )}     	  
  ) ~ { ctx => //println(ctx)	// Неопознаный запрос к серверу
	ctx.complete( StatusCodes.NotFound,  RespReq( Status.RnotFound, "", "") )
      } 
}
