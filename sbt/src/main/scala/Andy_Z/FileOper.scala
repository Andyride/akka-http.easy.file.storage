/*
 * Copyright (C) 2019-2020 Andrey Zaytsev andyride@yandex.ru
 */

package andyz.efs

import java.io.File
import spray.json.DefaultJsonProtocol._
import andyz.efs.UserHandler._
import scala.util.matching.Regex

import akka.http.scaladsl.server.directives._
import akka.http.scaladsl.server.Directives._


final case class RespFile(name: String, isfile: Int)
//  Список файлов
final case class RespFileSet( status: Statuscode, req: String, path: String, maxFileSize: Long, maxLengthFileName: Int, flist: Set[RespFile] )

//************************************* Обработка файлов
object FileOper {

    import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
//   JSON сериализация
  implicit val JFRespFile = jsonFormat2(RespFile)  
  implicit val JFRespFileSet = jsonFormat6(RespFileSet)

// ***************************** Получить содержимое каталога getName
  def dir(basepath: String, path: String):RespFileSet = {
  
//     val path=rawpath.trim().replaceAll("""/\.\.""", "")
    
    val d = new File( basepath + path  )
    var ListF: Set[RespFile] = Set.empty
     
    if (d.exists && d.isDirectory) {
	   d.listFiles.foreach( ff =>{
	      if(ff.isFile)ListF+=RespFile(ff.getName,1)
	      else ListF+=RespFile(ff.getName,0)	   
	   })
	RespFileSet( Status.Ok, "dir",  path, EFSConf.maxUploadSize, EFSConf.maxLengthFileName, ListF)   
    }else RespFileSet( Status.erReadDir, "dir",  path, EFSConf.maxUploadSize, EFSConf.maxLengthFileName, ListF)
  }   

//**********************************  Удалить файл или каталог
  def rm(basepath: String, path: String):RespReq = {   
  
//    val path=rawpath.trim().replaceAll("""/\.\.""", "") 
  
    try {
	var ErrorMess="" // Число ошибок удаления
	    
	    def deleteRecursively(file: File): Unit = {
		  if (file.isDirectory) {  file.listFiles.foreach(deleteRecursively)  }
		  if (file.exists && !file.delete) {   ErrorMess=file.getName()+","  }
	     }
	    deleteRecursively( new File(basepath + path) )
	if( !ErrorMess.isEmpty )RespReq( Status.rmFDErr, "rm", ErrorMess)
	else RespReq( Status.Ok, "rm", "")
    } catch {
    case e: Exception => RespReq( Status.rmFDErr, "rm", e.toString)// some file could not be deleted
    }
  }   
  
// ********************************************** Создать каталог
  def mkdir(basepath: String, path: String):RespReq = {  
  
//      val path=rawpath.trim().replaceAll("""/\.\.""", "")
  
     if( !CheckString(path,false) ) return RespReq( Status.erBadFileName, "mkdir", path)
     val dir = new File(basepath + path );  
     val successful = dir.mkdir();
     if(successful)  RespReq( Status.Ok, "mkdir", "")
     else RespReq( Status.erMkDir, "mkdir", path)
  }

//  **********************************  Загрузка файла
def UpLoad(basepath:String, rem:String) = {

	if( !CheckString(rem,false) ) complete( RespReq( Status.erBadFileName, "upload", rem))
	else{
	  val dirpath=basepath+rem
	  try{  def DestDir(fileInfo: FileInfo): File =  new File(dirpath+"/"+ fileInfo.fileName)		
		 var errmess=""
		 var ll:Long=0
		 storeUploadedFiles("files", DestDir)
		      { files =>
		      files.foreach { case (metadata, file) => 
				      val name=file.getName()				      
				      ll=file.length()
// 				      println("Upload "+name)
				      if(  !CheckString(name, true) ) { errmess+=s"$name,"; file.delete() }
				    }
		      if( errmess.isEmpty ) complete(RespReq( Status.Ok, "upload", "" )) 
		      else complete(RespReq( Status.erUpload, "upload", errmess))
		      }
	  }catch { case e: Exception => complete( RespReq( Status.erUpload, "upload", e.toString) ) }
	}
  } 

  
// ***************************** Проверка имени файла или каталога
  def CheckString(parm: String, isFile:Boolean):Boolean = {  
//   Ограничение длины имени
       if(parm.length>EFSConf.maxLengthFileName) return false
//        check pattern
       val Patt: Regex = if (isFile) raw"""[:;<>~%\!\"\'\`\*\\\/]""".r // File
			 else raw"""[:;<>~%\!\"\'\`\*]""".r // Dir
	
	Patt.findFirstMatchIn(parm) match {
		  case Some(_) => false
		  case None => true
	 }
  }
}
