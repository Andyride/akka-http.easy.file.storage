/*
 * Copyright (C) 2019-2020 Andrey Zaytsev andyride@yandex.ru
 */

package andyz.efs

import akka.actor.typed.ActorRef
import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.server.directives._
import spray.json.DefaultJsonProtocol._
import andyz.efs.FileOper._

//case-classes 
final case class User(name: String, password: String, gid: Int, lock: Int)
//  Список пользователей
final case class Users(users:Set[User])
// Сообщение о результате обработки данных
final case class Statuscode( code: Int, message: String ) 
 // Типовой ответ
final case class RespReq( status: Statuscode, req: String, message: String)
 // Ответ на запрос по пользователю
final case class RespUsr( status: Statuscode, req: String, user: User)
 // Ответ на запрос по списку пользователей
final case class RespUsrL( status: Statuscode, req: String, users: Set[User] )

//  Актор для работы с пользовательскими записями 
object UserHandler {

//   JSON сериализация
  implicit val JFUsr = jsonFormat4(User)  
  implicit val JFStCode = jsonFormat2(Statuscode)
  implicit val JFRespreq = jsonFormat3(RespReq)
  implicit val JFRespUsr = jsonFormat3(RespUsr)
  implicit val JFRespUsrL = jsonFormat3(RespUsrL)
  
// Запросы
  sealed trait Command
  final case class CreateUser(user: User, admin:Boolean, replyTo: ActorRef[RespUsr]) extends Command
  final case class deleteUser(user: User, replyTo: ActorRef[RespUsr]) extends Command
  final case class changeUser(user: User, replyTo: ActorRef[RespUsr]) extends Command  
  
  final case class ListUsr( replyTo: ActorRef[RespUsrL]) extends Command  
  final case class Auth(name: String, pass:Credentials.Provided, cond:Int => Boolean, replyTo: ActorRef[RespUsr]) extends Command

  final case class HGetUserResponse(maybeUser: Option[User])
  final case class ActionPerformed(description: String)
  
  var MapUsers:Map[String,User] = Map.empty
  val EmptyUser=User("", "", 1, 0) 
  
//   Инициализация
  def Init = {
    MapUsers = Passwd.ReadPasswd(EFSConf.confDir+"/passwd")
  }
  
  def apply(): Behavior[Command] = handler

//   ************************ Обработка почты ***************************
  private def handler: Behavior[Command] =
    Behaviors.receiveMessage {
// 	Создать
      case CreateUser(mkusr, admin, replyTo) => // Создать
        replyTo ! MkUser(mkusr, admin, replyTo)
        Behaviors.same
//         Удалить 
      case deleteUser(rmuser, replyTo) => // Удалить
        replyTo ! DelUser(rmuser, replyTo)
        Behaviors.same
//         Изменить данные пользователя
      case changeUser(chuser, replyTo) => 
        replyTo ! ChangeUser(chuser, replyTo)
        Behaviors.same  
//         Список пользователей
      case ListUsr( replyTo) => // Получить список
        replyTo ! { RespUsrL(Status.Ok, "listusers", MapUsers.values.toSet) }
        Behaviors.same  
//       Аутентификация зюзера
      case Auth(name, pass, cond, replyTo) =>
        replyTo ! Enter(name, pass, cond, replyTo)
        Behaviors.same
    }
    
//   ***************************************************    
    private def TrimUser(user:User):User = { User(user.name.trim(), user.password.trim(), user.gid, user.lock) }


//******************************  Создать пользователя
    private def MkUser(rawuser:User, admin:Boolean, replyTo:ActorRef[RespUsr]):RespUsr = {
// 	
	val user=TrimUser(rawuser)
// 	  Exist user
	if ( MapUsers.contains(user.name) ) RespUsr(Status.UserExist, "mkuser", user ) 
	else {
	// 	Проверка на пустые значения	  
	  if(user.name.isEmpty || user.password.isEmpty) return RespUsr(Status.Empty, "mkuser", user)

// 	  Проверка на правильность заполнения имени
	  if( !CheckString(user.name, true) ) return RespUsr(Status.BadName, "mkuser", user)

// 	  Проверка на правильность заполнения пароля
	  if( !CheckString(user.password, false) ) return RespUsr(Status.BadPass, "mkuser", user)

// 	  Создать каталог пользователя
	  val MkHomedir=mkdir(EFSConf.usersDir,"/"+user.name)
	  if(MkHomedir.status.code>0) RespUsr(MkHomedir.status, "mkuser", user) 
	
	  val mk= {if(admin) user else User(user.name, user.password, 1, 0) }
	  MapUsers+=(mk.name -> mk )
	  Passwd.WritePasswd(EFSConf.confDir+"/passwd",MapUsers)
	  RespUsr(Status.Ok, "mkuser", mk)
	}
    }
    
//***************************     Удалить пользователя
    private def DelUser(rawuser:User, replyTo:ActorRef[RespUsr]):RespUsr = {
	val user=TrimUser(rawuser)
	MapUsers-=user.name
	Passwd.WritePasswd(EFSConf.confDir+"/passwd",MapUsers)
// 	Удалить каталог пользователя
	val Homedir=rm(EFSConf.usersDir,"/"+user.name)
	RespUsr(Status.Ok, "rmuser", user ) 
    }
    
// *********************************    Изменить данные пользователя
    private def ChangeUser(rawuser:User, replyTo:ActorRef[RespUsr]):RespUsr = {
	val user=TrimUser(rawuser)
	// 	  Проверка на правильность заполнения пароля
	if( !CheckString(user.password, false) ) return RespUsr(Status.BadPass, "changeuser", user)
	  
	if (MapUsers.contains(user.name) ){
	    MapUsers+=(user.name -> user)
	    Passwd.WritePasswd(EFSConf.confDir+"/passwd",MapUsers)
	    RespUsr(Status.Ok, "changeuser", user)	
	}else RespUsr(Status.RnotFound, "changeuser", user)
    }
    
// ******************************************    Аутентификация через HTTP заголовок
    private def Enter(name:String, pass:Credentials.Provided, cond:Int => Boolean, replyTo:ActorRef[RespUsr]):RespUsr = {
	    val trimName=name.trim()
	    MapUsers.get(trimName) match {
		case Some(x) => { x match {
				  case lock if x.lock==1 => RespUsr(Status.UserLock, "enter",EmptyUser )
				  case password if pass.verify(x.password) => {					  
					if( cond(x.gid) ){ RespUsr(Status.Ok, "enter", x ) }
					else RespUsr(Status.NoRight, "enter", EmptyUser )
					}
				  case _ => RespUsr(Status.NoPass, "enter", EmptyUser ) 
				  }
				}
		case _ => RespUsr(Status.NoName, "enter", User(name, "", 1, 0)  ) 
		}
    }
    
}
//actor
