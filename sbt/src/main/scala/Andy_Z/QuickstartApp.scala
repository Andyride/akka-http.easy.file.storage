/*
 * Copyright (C) 2019-2020 Andrey Zaytsev andyride@yandex.ru
 */

package andyz.efs

import com.typesafe.config.ConfigFactory
import java.io._
import akka.actor.typed.{ ActorRef, ActorSystem, Behavior }
import akka.actor.typed.scaladsl.adapter._
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import scala.util.{ Failure, Success }


//#main-class
object QuickstartApp {
  //#start-http-server
  
  private def startHttpServer(routes: Route, system: ActorSystem[_]): Unit = {
    // Akka HTTP still needs a classic ActorSystem to start
    implicit val classicSystem: akka.actor.ActorSystem = system.toClassic
    import system.executionContext        
        
    val futureBinding = Http().bindAndHandle(routes, EFSConf.addr, EFSConf.port.toInt)
    futureBinding.onComplete {
      case Success(binding) =>
        val address = binding.localAddress
//         system.log.info("Server online at http://{}:{}/", address.getHostString, address.getPort)
      case Failure(ex) =>
        system.log.error("Failed to bind HTTP endpoint, terminating system", ex)
        system.terminate()
    }
  }
  //#start-http-server
  def main(args: Array[String]): Unit = {
      //       Get config system
      if( !EFSConf.GetConfig() )return      
      
    //#server-bootstrapping
    val rootBehavior = Behaviors.setup[Nothing] { context =>

      val UserHandlerActor = context.spawn(UserHandler(), "UserHandlerActor")
      context.watch(UserHandlerActor)
            
      val routes = new BaseRoute(UserHandlerActor)(context.system)
      startHttpServer(routes.userRoutes, context.system)

      Behaviors.empty      
    }
    
   val system = ActorSystem[Nothing](rootBehavior, "AkkaHttpServer") 
   		
  }
}
//#main-class

// Конфигурация системы
object EFSConf { 
 val config = ConfigFactory.load()    
 var appDir=""
 var webDir="web"
 var confDir="conf"
 var usersDir="users"
 var maxUploadSize:Long = 0
 var maxLengthFileName = 128
 var Separator=""
//  var FileEnc = ""
 var addr="0.0.0.0"
 var port="8080"
 
//***************************  Формирует параметры конфигурации, если не удалось делаем stop
 def GetConfig():Boolean = {
    
    val path = System.getProperty("user.dir")
//     println(path)
    Separator=System.getProperty("file.separator")
            
    var rez=true    
      //     Get configuration app
    try { appDir=config.getString("efs.data-parameters.app-dir")	
    } catch { case e: Exception => appDir=System.getProperty("user.dir")  }
    
    try{ usersDir= config.getString("efs.data-parameters.users-dir")    
    }catch{ case e: Exception => usersDir = appDir+Separator+"Users" }

//     
    if(rez){
      webDir = appDir+Separator+"web"
      confDir = appDir+Separator+"conf"
            
      try{ 
       maxUploadSize=config.getBytes("akka.http.parsing.max-content-length")
      }catch{ case e: Exception => maxUploadSize = 100*1024*1024 }
      
      println("\nApplication directory: "+appDir) //println("Каталог приложения: "+appDir)
      println("Users directory: "+usersDir) //println("Каталог данных: "+usersDir)
      println("Max size upload file: "+maxUploadSize/(1024*1024)+"Mb") //println("Максимальный размер загружаемого файла: "+maxUploadSize)
      
      try{ maxLengthFileName= config.getInt("efs.data-parameters.max-length-file-name")    
      }catch{ case e: Exception =>  maxLengthFileName=128 }
      println("Max length file name: "+maxLengthFileName+" symbols")
      
      try{ addr= config.getString("efs.data-parameters.saddress")    
      }catch{ case e: Exception =>  addr="0.0.0.0" }
      try{ port= config.getString("efs.data-parameters.port")    
      }catch{ case e: Exception => port="8080" }
      
      println(s"Address and port $addr:$port")
      Init()
    }else rez
//     println("\n")
  }

// ****************************** Инициализация системы
 def Init():Boolean = {
    var i=0
//     Проверка структуры системы
    val Starr=Array(appDir,webDir,confDir,usersDir)
    Starr.foreach{ el =>
	val dir = new File(el);
//      Не существует
	if(!dir.exists){ dir.mkdir() }
  //     Уже есть файл с таким именем
	if( !dir.isDirectory ) {i+=1; println("Error creating directory "+el)}	// println("Не могу создать каталог "+el)
    }
    if(i>0) return false
//     Прверка файла passwd
    val passwd = new File(confDir+"/passwd");
    if(!passwd.exists){ //println(s"Отсутствует файл $confDir/passwd"); return false
      Passwd.WritePasswd(confDir+"/passwd", Map("admin" -> User("admin","admin",0,0) ) )
      println(s"Make $confDir/passwd, user admin, password admin") //println(s"Создан файл $confDir/passwd, пользователь admin, пароль admin");
    }
    
//     passwd есть инициализируем список пользователей
    UserHandler.Init

 //     Проверка пользовательских каталогов
    UserHandler.MapUsers.foreach{ case (key,v) =>
	    val dir = new File(usersDir+"/"+key);
	    if(!dir.exists){ print("Missing user directory "+key+"  ..... creating  ") // print("Отсутствует каталог пользователя "+key+"  ..... создаем  ")  
			    if( dir.mkdir() ) println("Ok") else { i+=1; println("error !!!!") }
	    }
    }
    if(i>0) return false
//     Сделаем stop file
    val osName=System.getProperty("os.name")    
    println(s"OS : $osName")
    System.setProperty("file.encoding","UTF-8")    
//     FileEnc=System.getProperty("file.encoding")
//     println(s"File name encoding : $FileEnc")
//     import scala.collection.JavaConverters._
//     val prop = System.getProperties().asScala
//     for ((k,v) <- prop) println(s"key: $k, value: $v")
     
    val mypid=java.lang.management.ManagementFactory.getRuntimeMXBean().getName().split("@")(0);
    val Patt = "Windows".r
    val fileName = Patt.findFirstIn(osName) match{
	  case Some(x) => "stop.bat"
	  case _ => "stop.sh"
    }
//     val fileName=if(osName=="Windows")"stop.bat" else "stop.sh"
    
    val StopFile=new File( fileName )
    try {  
	    val stopf = new PrintWriter(StopFile)
	    if(fileName=="stop.bat"){ stopf.write("taskkill /f /PID "+mypid+"\n") }
	    else{
		  stopf.write("#!/bin/sh\n")
		  stopf.write("kill "+mypid+"\n")
	    }
	    stopf.close
	    StopFile.setExecutable(true)
	    scala.sys.addShutdownHook( StopFile.delete() )
	    true
      }catch {	case ex:Exception => println(ex.getMessage); false   }
    
 } 
}

// Статус обработки данных
object Status {
  val Ok=Statuscode	( 0, "Ok" ) // 0 - Ошибок нет,
  val NoName=Statuscode ( 1, "No such user " ) // "Неверное имя "
  val BadName=Statuscode( 2, "Bad name " ) // "Плохое имя "
  val NoPass=Statuscode	( 3, "Wrong password " ) // Неверный пароль
  val BadPass=Statuscode( 4, "Bad password " ) // Плохой пароль 
  val Empty=Statuscode	( 5, "Empty value" ) //"Пустое имя или парль не допускается"
  val NoRight=Statuscode( 6, "No permissions" ) // Нет прав доступа
  val UserLock=Statuscode  ( 7, "User is locked" ) // Пользователь заблокирован
  val UserExist=Statuscode ( 8, "User is exist" ) // Пользователь уже существует
  val RnotFound=Statuscode( 9, "Resource not found " ) // Ресурс не найден 
  val rmFDErr=Statuscode  ( 10, "Delete Error " ) // Ошибка удаления 
  val erBadFileName=Statuscode( 11, "Invalid file name " ) // Недопустимое имя файла
  val erMkDir=Statuscode(   12, "Error creating directory " ) // Ошибка создания каталога
  val erReadDir=Statuscode( 13, "Error reading directory " ) // Ошибка чтения каталога
  val erUpload=Statuscode( 14, "Error upload file " ) // Ошибка загрузки файла
}

